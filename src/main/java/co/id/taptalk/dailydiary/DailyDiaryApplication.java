package co.id.taptalk.dailydiary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@EntityScan(basePackages = {"co.id.taptalk.dailydiary.entity"})
@EnableJpaRepositories(basePackages = {"co.id.taptalk.dailydiary.repository"})
@SpringBootApplication
public class DailyDiaryApplication {

    public static void main(String[] args) {
        SpringApplication.run(DailyDiaryApplication.class, args);
    }

}
